FROM docker:dind

RUN apk add --no-cache bash && \
    apk add --no-cache openssh-client && \
    apk add --no-cache python3 && \
    apk add --no-cache python3-dev && \
    apk add --no-cache py3-pip && \
    apk add --no-cache gcc && \
    apk add --no-cache git && \
    apk add --no-cache curl && \
    apk add --no-cache build-base && \
    apk add --no-cache autoconf && \
    apk add --no-cache automake && \
    apk add --no-cache py3-cryptography && \
    apk add --no-cache linux-headers && \
    apk add --no-cache musl-dev && \
    apk add --no-cache libffi-dev && \
    apk add --no-cache openssl-dev && \
    apk add --no-cache openssh && \
    apk add --no-cache rust && \
    apk add --no-cache cargo && \
    python3 -m pip install --upgrade pi && \
    python3 -m pip install tox

